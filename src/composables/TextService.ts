import axios from 'axios';
import { UserModel } from '@/models/UserModel';
import {TextModel} from "@/models/TextModel";

export function useTextService() {
    return {
        async sendText(text: TextModel): Promise<TextModel> {
            const response = await axios.post<TextModel>('http://localhost:8080/send/message', text);
            return response.data;
        }
    };
}
