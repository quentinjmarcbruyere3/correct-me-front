import axios from 'axios';
import {ClassementModel} from "@/models/ClassementModel";

export function useClassementService() {
    return {
        async classmement(): Promise<Array<ClassementModel>> {
            const response = await axios.get<Array<ClassementModel>>('http://localhost:8080/classement/general', { params: { status: 'PENDING' } });
            return response.data;
        },
        async classmementOrtrhographe(): Promise<Array<ClassementModel>> {
            const response = await axios.get<Array<ClassementModel>>('http://localhost:8080/classement/orthographe', { params: { status: 'PENDING' } });
            return response.data;
        },
        async classmementGrammaire(): Promise<Array<ClassementModel>> {
            const response = await axios.get<Array<ClassementModel>>('http://localhost:8080/classement/grammar', { params: { status: 'PENDING' } });
            return response.data;
        }
    };
}
