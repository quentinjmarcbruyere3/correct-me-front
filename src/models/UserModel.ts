export interface UserModel {
    birthYear: number;
    login: string;
    password: string;
}
