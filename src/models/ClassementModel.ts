export interface ClassementModel {
    id: number,
    auteur: string;
    titre: string;
    texte: string;
    grammaire: number;
    orthographe: number;
    total: number;
}
