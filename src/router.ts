import { createWebHistory, createRouter } from 'vue-router';
import CorrectionPage from '@/views/CorrectionPage.vue';
import Home from '@/views/Home.vue';
import ClassementPage from '@/views/ClassementPage.vue';
import ClassementOrthographe from '@/views/ClassementOrthographe.vue';
import ClassementGrammaire from '@/views/ClassementGrammaire.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/correction',
            name: 'correction',
            component: CorrectionPage
        },
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/classement',
            name: 'classement',
            component: ClassementPage
        },
        {
            path: '/classement-orthographe',
            name: 'classement-orthographe',
            component: ClassementOrthographe
        },
        {
            path: '/classement-grammaire',
            name: 'classement-grammaire',
            component: ClassementGrammaire
        }
    ],
});
export default router;
